package me.danielzgtg.compsci11_sem2.calories;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
/**
 * A program for computing the total Calories of a meal
 * 
 * @author Daniel Tang
 * @since 5 March 2017
 */
public final class CalorieCounter implements Runnable {

	/**
	 * The number of non-"none" choices in the menu
	 */
	private static final int REAL_CHOICES_SIZE = 3;

	/**
	 * The introduction message layout
	 */
	private static final String INRODUCTION = "Welcome to Chip's Fast Food Emporium";

	/**
	 * The prompt layout for meal type choices
	 */
	private static final String PROMPT_LAYOUT = "Please enter a %s choice:";

	/**
	 * The error message when the intended selection could not be determined
	 */
	private static final String ERR_SELECTION_INVALID = "That selection could not be understood, please try again.";

	/**
	 * The header for the selections for a certain meal item type
	 */
	private static final String MENU_TITLE_LAYOUT = "Here are the %s %s choices:\n";

	/**
	 * The menu item entry layout
	 */
	private static final String MENU_ITEM_LAYOUT = "%d \u2013 %s (%d Calories)\n";

	/**
	 * The "none" menu entry layout
	 */
	private static final String MENU_ITEM_NONE_LAYOUT = REAL_CHOICES_SIZE + 1 + " \u2013 no %s\n";

	/**
	 * The output layout for the total calories
	 */
	private static final String OUTPUT_LAYOUT = "Your total calorie count is %d.\n";

	/**
	 * The menu of types, choices, and calories
	 */
	private static final List<Pair<String, List<Pair<String, Integer>>>> CALORIE_DATA =
			Collections.unmodifiableList(Arrays.asList(
					new Pair<>("burger", Collections.unmodifiableList(Arrays.asList(
							new Pair<>("Cheeseburger", 461),
							new Pair<>("Fish Burger", 431),
							new Pair<>("Veggie Burger", 420)
							))),
					new Pair<>("side order", Collections.unmodifiableList(Arrays.asList(
							new Pair<>("Fries", 100),
							new Pair<>("Baked Potato", 57),
							new Pair<>("Chef Salad", 70)
							))),
					new Pair<>("drink", Collections.unmodifiableList(Arrays.asList(
							new Pair<>("Soft Drink", 130),
							new Pair<>("Orange Juice", 160),
							new Pair<>("Milk", 118)
							))),
					new Pair<>("desert", Collections.unmodifiableList(Arrays.asList(
							new Pair<>("Apple Pie", 167),
							new Pair<>("Sundae", 266),
							new Pair<>("Fruit cup", 75)
							)))
					));

	/**
	 * The number of calories counted so far
	 */
	private int countedCalories = 0;

	@Override
	public final void run() {
		System.out.println(INRODUCTION); // Say Hi

		try (final Scanner scanner = new Scanner(System.in)) { // Init an auto-cleanup scanner
			// Get the list of entries for a meal component type, for each
			for (final Pair<String, List<Pair<String, Integer>>> dataForOneType : CALORIE_DATA) {
				countCaloriesForChoiceType(dataForOneType.left, dataForOneType.right, scanner); // Prompt and count calories for it
			}
		}

		System.out.format(OUTPUT_LAYOUT, countedCalories); // Output result
	}

	/**
	 * Counts the calories for a meal component type by
	 * showing the user the list of available choices,
	 * obtaining an entry,
	 * and counting the calories in the entry.
	 * 
	 * @param type The name of the meal  component type
	 * @param selections The available choices for the selection type
	 * @param scanner The scanner to use for user input
	 */
	private final void countCaloriesForChoiceType(
			final String type, final List<Pair<String, Integer>> selections, final Scanner scanner) {
		introduceChoiceSelectionsForType(type, selections); // Show menu

		while (true) { // Keep trying until good
			System.out.format(PROMPT_LAYOUT, type); // Show prompt

			try {
				final int index = scanner.nextInt(); // Obtain user input index
				countedCalories += // Count the calories
						index == REAL_CHOICES_SIZE + 1 // If it is the end, then it is the "none" entry
						? 0 // No calories if they choose nothing
								: selections.get(index - 1) // Get the selected entry (convert from user one-indexed to Java zero-indexed)
								.right; // Get the calories component
				return;
			} catch (final Exception e) {
				System.out.println(ERR_SELECTION_INVALID); // Complain
			} finally {
				scanner.nextLine(); // Get scanner ready again
			}
		}
	}

	/**
	 * Shows the menu of selections for a meal component type.
	 * 
	 * @param type The name of the meal component type
	 * @param selections The list of selections in the meal component type
	 */
	@SuppressWarnings("all")
	private final void introduceChoiceSelectionsForType(final String type, final List<Pair<String, Integer>> selections) {
		System.out.format(MENU_TITLE_LAYOUT, REAL_CHOICES_SIZE == 3 ? "three" : "", type);

		assert selections.size() == REAL_CHOICES_SIZE; // Sizes need to be the same to prevent glitches
		for (int i = 0; i < selections.size(); i++) { // Loop through the menu selections
			final Pair<String, Integer> currentItem = selections.get(i); // Get the selection
			System.out.format(MENU_ITEM_LAYOUT, // Print out the item in style
					i + 1, // Convert from Java zero-indexed to user one-indexed
					currentItem.left, // Print the name
					currentItem.right); // Print the calories in it
		}

		System.out.format(MENU_ITEM_NONE_LAYOUT, type); // Show the "none" option
	}

	public static void main(final String[] ignore) {
		new CalorieCounter().run(); // Calorie Counter needs to be an Object to run
	}

}
