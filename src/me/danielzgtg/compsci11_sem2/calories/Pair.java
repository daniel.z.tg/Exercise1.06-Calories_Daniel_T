package me.danielzgtg.compsci11_sem2.calories;

import java.util.Objects;

/**
 * A generic pair class to be used for various purposes.
 * 
 * @author Daniel Tang
 * @since 5 March 2017
 *
 * @param <L> The type for the left field
 * @param <R> The type for the right field
 */
public class Pair<L, R> {

	/**
	 * The left object in the pair
	 */
	public final L left;
	
	/**
	 * The right object in the pair
	 */
	public final R right;

	/**
	 * Creates a immutable object pairing
	 * 
	 * @param left The left-side object to be paired
	 * @param right The right-side object to be paired
	 */
	public Pair(L left, R right) {
		this.left = left;
		this.right = right;
	}

	/**
	 * Gets the left object in the pair
	 * 
	 * @return The left object
	 */
	public final L getLeft() {
		return left;
	}

	/**
	 * Get the right object in the pair
	 * 
	 * @return The right object
	 */
	public final R getRight() {
		return right;
	}

	/**
	 * Computes a hash code for this pair.
	 * Implemented as just a generic hash using the left and right components.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		return Objects.hash(left, right);
	}

	/**
	 * Determines if this pair is equal to another object
	 * Implemented as just checking if the other object is a pair, and the contained objects are equal
	 * 
	 * @see java.lang.Object#equals()
	 */
	@Override
	public final boolean equals(final Object other) {
		if (!(other instanceof Pair)) return false;
		final Pair<?, ?> otherPair = (Pair<?, ?>) other;

		return Objects.equals(otherPair.left, otherPair.right);
	}

	/**
	 * Clones the object.
	 * This just shallowly creates another pair with the same contents.
	 */
	@Override
	public Object clone() {
		return new Pair<L, R>(left, right);
	}

	/**
	 * Gets a String representation of the pair.
	 * 
	 * The String returned is the {@code toString()} of a generic object
	 * appended to a set with the left hashcode, with indicators,
	 * and the right hashcode, the same.
	 */
	@Override
	public String toString() {
		return new StringBuilder().append(super.toString())
				.append("{ L:").append(String.valueOf(left))
				.append(" R:").append(String.valueOf(right))
				.append(" }").toString();
	}
}
