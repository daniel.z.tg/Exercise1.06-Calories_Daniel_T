package me.danielzgtg.compsci11_sem2.calories;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * A debug tool for fixing the Calorie Counter. Not used anymore. Please don't use.
 * 
 * @author Daniel Tang
 */
@SuppressWarnings("unchecked")
public final class CalorieDebug {

	/**
	 * Menu from CalorieCounter
	 */
	private static final List<Pair<String, List<Pair<String, Integer>>>> CALORIE_DATA;

	static {
		// This is for debug only. It uses magic. Please don't look at it.
		try {
			final Field f = CalorieCounter.class.getDeclaredField("CALORIE_DATA");
			f.setAccessible(true);
			CALORIE_DATA = (List<Pair<String, List<Pair<String, Integer>>>>) f.get(null);
		} catch (final Throwable t) {
			throw new RuntimeException(t);
		}
	}

	public static void main(final String[] ignore) {
		// This is for debug only. It uses magic. Please don't look at it.
		Stream.concat(CALORIE_DATA.get(0).right.stream().map(Pair::getRight),
				Arrays.asList(0).stream()).forEach(a ->
				Stream.concat(CALORIE_DATA.get(1).right.stream().map(Pair::getRight),
						Arrays.asList(0).stream()).forEach(b -> 
						Stream.concat(CALORIE_DATA.get(2).right.stream().map(Pair::getRight),
								Arrays.asList(0).stream()).forEach(c ->
								Stream.concat(CALORIE_DATA.get(3).right.stream().map(Pair::getRight),
										Arrays.asList(0).stream()).forEach(d ->
										System.out.format("%4d %4d %4d %4d = %d\n", a, b, c, d, a+b+c+d)
												))));
	}
}
